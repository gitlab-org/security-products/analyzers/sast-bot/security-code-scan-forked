package cwe

import (
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/report/v5"
)

func TestCodeToSeverity(t *testing.T) {
	tests := []struct {
		code     string
		severity report.SeverityLevel
	}{
		{
			code:     "SCS0001",
			severity: report.SeverityLevelCritical,
		},
		{
			code:     "SCSBAAAD",
			severity: report.SeverityLevelUnknown,
		},
	}

	for _, tt := range tests {
		got := CodeToSeverity(tt.code)
		if got != tt.severity {
			t.Errorf("CodeToSeverity(%s) = %s, want %s", tt.code, got, tt.severity)
		}
	}
}
