require 'gitlab_secure/integration_test/docker_runner'
require 'gitlab_secure/integration_test/shared_examples/scan_shared_examples'
require 'gitlab_secure/integration_test/shared_examples/report_shared_examples'
require 'gitlab_secure/integration_test/spec_helper'

describe 'running image' do
  let(:fixtures_dir) { 'qa/fixtures' }
  let(:expectations_dir) { 'qa/expect' }

  def target_mount_dir
    '/app'
  end

  def image_name
    ENV.fetch('TMP_IMAGE', 'security-code-scan:latest')
  end

  def privileged
    ENV.fetch('PRIVILEGED', 'true') == 'true'
  end

  context 'with no project' do
    let(:output) { `docker run -t --rm -w #{target_mount_dir} -e CI_PROJECT_DIR=#{target_mount_dir} #{image_name}` }
    let(:exit_code) { $CHILD_STATUS.to_i }

    it 'shows there is no match' do
      expect(output).to match(/no match in #{target_mount_dir}/i)
    end

    describe 'exit code' do
      specify { expect(exit_code).to be 0 }
    end
  end

  context 'with test project' do
    def parse_expected_report(expectation_name)
      path = File.join(expectations_dir, expectation_name, 'gl-sast-report.json')
      JSON.parse(File.read(path))
    end

    def sorted_report(report)
      (report['vulnerabilities'] || []).sort_by! do |v|
        # The report package sorts vulnerabilities by Severity, CompareKey, and Location.Dependency.Version.
        # (see https://gitlab.com/gitlab-org/security-products/analyzers/report/blob/2bf0bce/report.go#L107-107)
        #
        # However, some of the webgoat vulnerabilities have the same severity and comparekey value, which means
        # that their order is non-deterministic. We sort by ID to ensure that the order of the vulnerabilites
        # is deterministic.
        #
        # TODO: update the report package to also sort by Vulnerabilities[].Name and remove this workaround.
        # See https://gitlab.com/gitlab-org/gitlab/-/issues/467361 for more details.
        v['id']
      end
      report
    end

    let(:global_vars) do
      {
        'ANALYZER_INDENT_REPORT': 'true',
        'CI_PROJECT_DIR': target_mount_dir,
        'SEARCH_MAX_DEPTH': 5  # the sources in some fixture directories are heavily nested
      }
    end

    let(:project) { 'any' }
    let(:variables) { {} }
    let(:command) { [] }
    let(:script) { nil }
    let(:offline) { false }
    let(:target_dir) { File.join(fixtures_dir, project) }

    let(:scan) do
      GitlabSecure::IntegrationTest::DockerRunner.run_with_cache(
        image_name, fixtures_dir, target_dir, @description,
        command: command,
        script: script,
        offline: offline,
        privileged: privileged,
        variables: global_vars.merge(variables),
        report_filename: 'gl-sast-report.json')
    end

    let(:report) { scan.report }

    context 'csharp-dotnetcore-multiproject' do
      let(:report) { scan.report }

      context 'default' do
        let(:project) { 'csharp-dotnetcore-multiproject/default' }

        it_behaves_like 'successful scan'

        describe 'created report' do
          it_behaves_like 'non-empty report'

          it_behaves_like 'recorded report' do
            let(:recorded_report) { parse_expected_report(project) }
          end

          it_behaves_like 'valid report'
        end
      end

      context 'multisolution' do
        let(:project) { 'csharp-dotnetcore-multiproject/multisolution' }

        it_behaves_like 'successful scan'

        describe 'created report' do
          it_behaves_like 'non-empty report'

          it_behaves_like 'recorded report' do
            let(:recorded_report) { parse_expected_report(project) }
          end

          it_behaves_like 'valid report'
        end
      end

      context 'msbuild' do
        let(:project) { 'csharp-dotnetcore-multiproject/multisolution-msbuild' }

        it_behaves_like 'successful scan'

        describe 'created report' do
          it_behaves_like 'non-empty report'

          it_behaves_like 'recorded report' do
            let(:recorded_report) { parse_expected_report(project) }
          end

          it_behaves_like 'valid report'
        end
      end
    end

    context 'dotnet5' do
      let(:project) { 'dotnet5' }

      context 'by default' do
        it_behaves_like 'successful scan'

        describe 'created report' do
          it_behaves_like 'non-empty report'

          it_behaves_like 'recorded report' do
            let(:recorded_report) { parse_expected_report(project) }
          end

          it_behaves_like 'valid report'
        end
      end
    end

    context 'dotnet6' do
      let(:project) { 'dotnet6' }

      context 'by default' do
        it_behaves_like 'successful scan'

        describe 'created report' do
          it_behaves_like 'non-empty report'

          it_behaves_like 'recorded report' do
            let(:recorded_report) { parse_expected_report(project) }
          end

          it_behaves_like 'valid report'
        end
      end
    end

    context 'webgoat.net' do
      let(:project) { 'webgoat.net' }
      let(:report) { sorted_report(scan.report) }

      context 'by default' do
        it_behaves_like 'successful scan'

        describe 'created report' do
          it_behaves_like 'non-empty report'

          it_behaves_like 'recorded report' do
            let(:recorded_report) { sorted_report(parse_expected_report(project)) }
          end

          it_behaves_like 'valid report'
        end
      end
    end
  end
end
