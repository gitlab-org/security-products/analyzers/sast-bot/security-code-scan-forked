package project

import (
	"fmt"
	"io/ioutil"
	"os"
	"testing"
)

func TestNoEndingProjectTag(t *testing.T) {
	tmpFile, err := createTestFile("<a></a>")
	defer os.Remove(tmpFile.Name())
	defer tmpFile.Close()
	if err != nil {
		t.Fatalf("unable to create test xml file: %v\n", err)
	}

	err = InsertAnalyzersToProjFile(tmpFile.Name())
	if err == nil {
		t.Error("Expected to get error with bad xml file.")
	}
}

func TestInsertAnalyzersToProjFile(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name          string
		contentFormat string
	}{
		{"basic project",
			"<Project>\n%s</Project>\n",
		},
		{"project with configuration",
			"<Project>\n<ItemGroup>\n</ItemGroup>\n%s</Project>\n",
		},
		// This currently get's an ending newline added, but should be valid xml.
		// {"project configuration with no ending newline",
		// 	"<Project>\n<ItemGroup>\n</ItemGroup>\n%s</Project>",
		// },
		// Doesn't work due to current reliance on newlines
		// {"project configuration with no newlines",
		// 	"<Project><ItemGroup></ItemGroup>%s</Project>",
		// },
		// Doesn't work due to current reliance on \n newlines
		// {"project configuration with \\r newlines",
		// 	"<Project>\r<ItemGroup>\r</ItemGroup>\r%s</Project>\r",
		// },
	}

	for _, tt := range tests {
		tt := tt // NOTE: https://github.com/golang/go/wiki/CommonMistakes#using-goroutines-on-loop-iterator-variables
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			content := fmt.Sprintf(tt.contentFormat, "")
			expected := fmt.Sprintf(tt.contentFormat, analyzersXML)

			tmpFile, err := createTestFile(content)
			defer os.Remove(tmpFile.Name())
			defer tmpFile.Close()
			if err != nil {
				t.Fatalf("unable to create test xml file: %v\n", err)
			}

			err = InsertAnalyzersToProjFile(tmpFile.Name())
			if err != nil {
				t.Errorf("Unexpected error: %v\n", err)
			}

			gotB, err := ioutil.ReadFile(tmpFile.Name())
			if err != nil {
				t.Fatalf("Could not read tmpFile to check results, err: %v\n", err)
			}

			got := string(gotB)

			if expected != got {
				t.Errorf("Expected match to be\n%#v\nbut got\n%#v", expected, got)
			}
		})
	}
}

func createTestFile(content string) (*os.File, error) {
	tmpFile, err := ioutil.TempFile("", "insertTest-*.xml")

	if err != nil {
		return nil, err
	}

	tmpFile.Write([]byte(content))

	return tmpFile, nil
}
