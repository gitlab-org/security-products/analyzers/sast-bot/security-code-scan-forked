package metadata_test

import (
	"reflect"
	"testing"

	report "gitlab.com/gitlab-org/security-products/analyzers/report/v5"
	"gitlab.com/gitlab-org/security-products/analyzers/security-code-scan/v3/metadata"
)

func TestReportScanner(t *testing.T) {
	want := report.ScannerDetails{
		ID:      "security_code_scan",
		Name:    "Security Code Scan",
		Version: metadata.ScannerVersion,
		Vendor: report.Vendor{
			Name: "GitLab",
		},
		URL: "https://security-code-scan.github.io",
	}
	got := metadata.ReportScanner

	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}
