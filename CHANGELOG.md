# security-code-scan .net analyzer changelog

## v3.15.0
- upgrade from `libssl1.1` to `libssl3` (!172)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v5` version [`v5.3.0` => [`v5.5.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v5.5.0)] (!172)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3` version [`v3.2.1` => [`v3.3.1`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v3.3.1)] (!172)

## v3.14.0
- upgrade `github.com/urfave/cli/v2` version [`v2.27.4` => [`v2.27.5`](https://github.com/urfave/cli/releases/tag/v2.27.5)] (!171)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/common/v3` version [`v3.3.0` => [`v3.4.0`](https://gitlab.com/gitlab-org/security-products/analyzers/common/-/releases/v3.4.0)] (!171)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3` version [`v3.2.0` => [`v3.2.1`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v3.2.1)] (!171)

## v3.13.0
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3` version [`v3.1.1` => [`v3.2.0`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v3.2.0)] (!169)

## v3.12.0
- update `go` version to `v1.22` (!165)
- upgrade `github.com/urfave/cli/v2` version [`v2.27.3` => [`v2.27.4`](https://github.com/urfave/cli/releases/tag/v2.27.4)] (!165)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v5` version [`v5.2.1` => [`v5.3.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v5.3.0)] (!165)

## v3.11.2
- upgrade `github.com/urfave/cli/v2` version [`v2.27.2` => [`v2.27.3`](https://github.com/urfave/cli/releases/tag/v2.27.3)] (!164)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/common/v3` version [`v3.2.3` => [`v3.3.0`](https://gitlab.com/gitlab-org/security-products/analyzers/common/-/releases/v3.3.0)] (!164)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3` version [`v3.1.0` => [`v3.1.1`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v3.1.1)] (!164)

## v3.11.1
- upgrade `github.com/urfave/cli/v2` version [`v2.27.1` => [`v2.27.2`](https://github.com/urfave/cli/releases/tag/v2.27.2)] (!163)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v5` version [`v5.1.0` => [`v5.2.1`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v5.2.1)] (!163)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3` version [`v3.0.0` => [`v3.1.0`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v3.1.0)] (!163)

## v3.11.0
- Update `command` package from `v2.2.0` to `v3.1.0` (!155)
- Update `report` package from `v4.3.2` to `v5.1.0` (!155)
- Update `ruleset` package from `v2.0.8` to `v3.0.0` (!155)

## v3.10.3
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.7` => [`v2.0.8`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.8)] (!154)

## v3.10.2
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.6` => [`v2.0.7`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.7)] (!153)

## v3.10.1
- upgrade `github.com/urfave/cli/v2` version [`v2.26.0` => [`v2.27.1`](https://github.com/urfave/cli/releases/tag/v2.27.1)] (!152)

## v3.10.0
- Update golang to v1.21 (!150)
- Update command, report, common, and ruleset golang modules (!150)
- Trim vulnerability name when length exceeds 255chars (!150)
- Drop deprecated vulnerability.message field (!150)

## v3.9.3
- upgrade `github.com/sirupsen/logrus` version [`v1.9.0` => [`v1.9.3`](https://github.com/sirupsen/logrus/releases/tag/v1.9.3)] (!146)
- upgrade `github.com/urfave/cli/v2` version [`v2.25.3` => [`v2.25.7`](https://github.com/urfave/cli/releases/tag/v2.25.7)] (!146)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/common/v3` version [`v3.2.2` => [`v3.2.3`](https://gitlab.com/gitlab-org/security-products/analyzers/common/-/releases/v3.2.3)] (!146)

## v3.9.2
- upgrade `github.com/urfave/cli/v2` version [`v2.25.1` => [`v2.25.3`](https://github.com/urfave/cli/releases/tag/v2.25.3)] (!138)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.10.2` => [`v1.10.3`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.3)] (!138)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.22.1` => [`v3.22.2`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.22.2)] (!138)

## v3.9.1
- upgrade `github.com/urfave/cli/v2` version [`v2.23.7` => [`v2.25.1`](https://github.com/urfave/cli/releases/tag/v2.25.1)] (!136)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.10.1` => [`v1.10.2`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.2)] (!136)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.17.0` => [`v3.22.1`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.22.1)] (!136)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset` version [`v1.4.0` => [`v1.4.1`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v1.4.1)] (!136)
- update Go version to 1.19 (!136)

## v3.9.0
- Add support for .NET 7.0 (!128)

## v3.8.7
- upgrade `github.com/urfave/cli/v2` version [`v2.23.6` => [`v2.23.7`](https://github.com/urfave/cli/releases/tag/v2.23.7)] (!132)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.10.0` => [`v1.10.1`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.1)] (!132)

## v3.8.6
- upgrade `github.com/urfave/cli/v2` version [`v2.23.5` => [`v2.23.6`](https://github.com/urfave/cli/releases/tag/v2.23.6)] (!129)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.16.0` => [`v3.17.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.17.0)] (!129)

## v3.8.5
- Change `log.Errof` to `log.Warnf` on dotnet build failures before returning error (!130)

## v3.8.4
- Ensure unsupported projects are not analysed (!131)

## v3.8.3
- upgrade `github.com/urfave/cli/v2` version [`v2.19.2` => [`v2.23.5`](https://github.com/urfave/cli/releases/tag/v2.23.5)] (!127)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.9.2` => [`v1.10.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.0)] (!127)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.15.2` => [`v3.16.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.16.0)] (!127)

## v3.8.2
- upgrade `github.com/sirupsen/logrus` version [`v1.8.1` => [`v1.9.0`](https://github.com/sirupsen/logrus/releases/tag/v1.9.0)] (!126)
- upgrade `github.com/urfave/cli/v2` version [`v2.16.3` => [`v2.19.2`](https://github.com/urfave/cli/releases/tag/v2.19.2)] (!126)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.8.2` => [`v1.9.2`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.9.2)] (!126)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/common/v3` version [`v3.2.1` => [`v3.2.2`](https://gitlab.com/gitlab-org/security-products/analyzers/common/-/releases/v3.2.2)] (!126)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.12.2` => [`v3.15.2`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.15.2)] (!126)

## v3.8.1
- Update common to `v3.2.1` to fix gotestsum cmd (!125)

## v3.8.0
- upgrade [`Security Code
  Scan`](https://github.com/security-code-scan/security-code-scan)
version [`5.6.0` =>
[`5.6.7`](https://github.com/security-code-scan/security-code-scan/releases/tag/5.6.7)]
(!124)
- upgrade `github.com/urfave/cli/v2` version [`v2.11.1` =>
  [`v2.16.3`](https://github.com/urfave/cli/releases/tag/v2.16.3)]
(!124)

## v3.7.0
- Update to use `SecurityCodeScan.VS2019` package instead of deprecated package. ([!122](https://gitlab.com/gitlab-org/security-products/analyzers/security-code-scan/-/merge_requests/122) @nathan_miller)

## v3.6.0
- upgrade [`Security Code Scan`](https://github.com/security-code-scan/security-code-scan) version [`5.6.0` => [`5.6.5`](https://github.com/security-code-scan/security-code-scan/releases/tag/5.6.5)] (!121)
- upgrade `github.com/sirupsen/logrus` version [`v1.8.1` => [`v1.9.0`](https://github.com/sirupsen/logrus/releases/tag/v1.9.0)] (!121)
- upgrade `github.com/urfave/cli/v2` version [`v2.11.0` => [`v2.11.1`](https://github.com/urfave/cli/releases/tag/v2.11.1)] (!121)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.8.2` => [`v1.9.1`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.9.1)] (!121)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.12.2` => [`v3.13.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.13.0)] (!121)

## v3.5.2
- Fix golangci-lint `prealloc` linter errors (!118)

## v3.5.1
- Upgrade the `command` package for better analyzer messages. (!119)

## v3.5.0
- Upgrade core analyzer dependencies (!117)
  + Adds support for globstar patterns when excluding paths
  + Adds analyzer details to the scan report

## v3.4.2
- Increase log level when `dotnet add` fails (!116)

## v3.4.1
- Update command to `v1.6.1` (!112)
- Update report to `v3.10.0` (!112)
- Update crypto to latest (!112)

## v3.4.0
- Update ruleset, report, and command modules to support ruleset overrides (!105)

## v3.3.0
- Drop support for .NET 2.1 (!107)

## v3.2.0
- Add severity level to report (!104)

## v3.1.1
- Update [security-code-scan](https://security-code-scan.github.io/#5.6.0) to v5.6.0 (!100)
  - Added support for .NET 6.0 and VS2022

## v3.1.0
- Add support for .NET 6.0 (!96)

## v3.0.3
- Update common to `v2.24.1` which fixes a git certificate error when using `ADDITIONAL_CA_CERT_BUNDLE` (!99)

## v3.0.2
- Upgrade to to v1.17 (!95)

## v3.0.1
- Fix: Gosec vulnerabilities (!91)

## v3.0.0
- Update [security-code-scan](https://security-code-scan.github.io/#5.2.1) to v5.2.1 (!80)
  - Add support for VS2019
  - Bug fixes and improvements

## v2.21.0
- Use alpine base image (!84)

## v2.20.3
- Additional debugging support (!88)

## v2.20.2
- Fix: failure with large input using bufio.Scanner (!82)

## v2.20.1
- Restrict permissions on created dirs in the Dockerfile (!79)

## v2.20.0
- Update Dockerfile to support Open Shift (!78)

## v2.19.0
- Update report dependency in order to use the report schema version 14.0.0 (!77)

## v2.18.2
- Fix index out of range panic in convert (!74)
- Skip SCS9999 warning (!74)

## v2.18.1
- Add additional debug and error messages in `analyzeProjectDotNet` (!73)

## v2.18.0
- Fixes failing scans for monorepos by introducing a more robust build algorithm for different project structures (!70)
- Update `analyze` to include `analyzeSolution` to support different project structures (!70)
- Split building of `msbuild` projects and `dotnet` projects into separate functions (!70)

## v2.17.0
- Add support for .NET 5.0 (!69 @shaun.burns)

## v2.16.1
- Update common to `v2.22.1` which fixes a CA Certificate bug when analyzer is run more than once (!68)

## v2.16.0
- Set `analyzeAll` to true in the command config (!66)
- Update `analyzeProjects` to support repos with multiple solutions (!66)

## v2.15.0
- Update mono base image to v6.12 (!67)
- Update logrus golang module to v1.7.0 (!67)

## v2.14.0
- Update common to v2.22.0 (!64)
- Update urfave/cli to v2.3.0 (!64)

## v2.13.0
- Update common and enabled disablement of rulesets (!58)

## v2.12.3
- Fix incorrect filepath for nested source files (!60)

## v2.12.2
- Update golang dependencies (!53)

## v2.12.1
- Use mono:6.10 container rather than mono:6.10-slim (!50)
- Update common to v2.16

## v2.12.0
- Update mono to v6.10 (!47)

## v2.11.0
- Add `scan.start_time`, `scan.end_time` and `scan.status` to report (!35)

## v2.10.1
- Upgrade go to version 1.15 (!45)

## v2.10.0
- Add scan object to report (!42)

## v2.9.0
- Switch to the MIT Expat license (!39)

## v2.8.1
- Fix `findProjects` log-level from error to debug (!38)

## v2.8.0
- Update logging to be standardized across analyzers (!37)

## v2.7.3
- Fix misuse of `SAST_EXCLUDED_PATHS` resulting in scanned projects being skipped (!36)

## v2.7.2
- Change the location of custom CA certs (!32)

## v2.7.1
- Remove `location.dependency` from the generated SAST report (!31)

## v2.7.0
- Update [security-code-scan](https://security-code-scan.github.io/#3.5.3) to v3.5.3 (!30)

## v2.6.0
- Change Docker image base to use mono (!29 @agixid)
- Support building .NET Framework projects (!29 @agixid)

## v2.5.0
- Add `id` field to vulnerabilities in JSON report (!26)

## v2.4.0
- Add support for custom CA certs (!24)

## v2.3.0
- Add support for .NET multiprojects

## v2.2.0
- Upgrade to .NET Core SDK 3.1 (LTS) (!19 @shaun.burns)

## v2.1.1
- Update the `dotnet clean` command to run after the analyzer adds the security package

## v2.1.0
- Update [security-code-scan](https://security-code-scan.github.io/#3.3.0) to v3.3.0.

## v2.0.3
- Print stderr in case of execution error (!12)

## v2.0.2
- Bump common to v2.1.6

## v2.0.1
- Bump common to v2.1.5

## v2.0.0
- Switch to new report syntax with `version` field

## v1.3.0
- Upgrade to .NET SDK 2.2

## v1.2.0
- Add `Scanner` property and deprecate `Tool`

## v1.1.0
- Show command error output

## v1.0.0
- initial release
